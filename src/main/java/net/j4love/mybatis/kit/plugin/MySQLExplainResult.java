package net.j4love.mybatis.kit.plugin;


import lombok.Data;

@Data
public class MySQLExplainResult {

    private Integer id;
    private String selectType;
    private String table;
    private String partitions;
    private String type;
    private String possibleKeys;
    private String key;
    private String keyLen;
    private String ref;
    private Long rows;
    private Double filtered;
    private String extra;

}
