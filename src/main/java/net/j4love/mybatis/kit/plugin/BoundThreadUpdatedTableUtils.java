package net.j4love.mybatis.kit.plugin;

import java.util.HashSet;
import java.util.Set;

public class BoundThreadUpdatedTableUtils {

    private static final ThreadLocal<Set<String>> THREAD_LOCAL = new ThreadLocal<>();

    public static Set<String> get() {
        return THREAD_LOCAL.get();
    }
    public static Set<String> getAndClear() {
        Set<String> tables = THREAD_LOCAL.get();
        clear();
        return tables;
    }

    public static void set(Set<String> tables) {
        THREAD_LOCAL.set(tables);
    }

    public static void append(Set<String> tables) {
        Set<String> oldTables = THREAD_LOCAL.get();
        Set<String> newTables = new HashSet<>(oldTables);
        newTables.addAll(tables);
        set(newTables);
    }


    public static void clear() {
        THREAD_LOCAL.remove();
    }

}
