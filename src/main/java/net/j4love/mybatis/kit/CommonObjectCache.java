package net.j4love.mybatis.kit;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CommonObjectCache {

    public static final String DATABASE_META_DATA_KEY = "database_meta_data";
    public static final String DATABASE_TYPE_KEY = "database_type";

    private static final Map<String , Object> CACHE_MAP = new ConcurrentHashMap<>();

    public static void put(String key , Object value) {
        CACHE_MAP.put(key , value);
    }

    public static Object get(String key) {
        return CACHE_MAP.get(key);
    }

}
