package net.j4love.mybatis.kit.sql;

import lombok.Builder;
import lombok.Data;
import org.apache.ibatis.mapping.SqlCommandType;

@Data
@Builder
public class TableCommand {

    private String tableName;
    private SqlCommandType sqlCommandType;
}
