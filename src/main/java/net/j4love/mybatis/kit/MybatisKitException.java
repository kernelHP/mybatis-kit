package net.j4love.mybatis.kit;

/**
 * @author he peng
 * @date 2018/9/10
 */
public class MybatisKitException extends RuntimeException {

    public MybatisKitException() {
        super();
    }

    public MybatisKitException(String message) {
        super(message);
    }

    public MybatisKitException(String message, Throwable cause) {
        super(message, cause);
    }

    public MybatisKitException(Throwable cause) {
        super(cause);
    }

    protected MybatisKitException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
