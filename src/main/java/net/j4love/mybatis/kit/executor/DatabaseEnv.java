package net.j4love.mybatis.kit.executor;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DatabaseEnv {

    private String url;
    private String envId;
}
