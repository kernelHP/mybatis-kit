package net.j4love.mybatis.kit.executor;

import org.apache.ibatis.executor.BaseExecutor;
import org.apache.ibatis.session.Configuration;

import java.lang.reflect.Field;

public class Executors {

    public static Configuration getConfigurationFromBaseExecutor(BaseExecutor target) {
        Configuration conf = null;
        Class<BaseExecutor> baseExecutorClass = BaseExecutor.class;
        try {
            Field configurationField = baseExecutorClass.getDeclaredField("configuration");
            configurationField.setAccessible(true);
            conf = (Configuration) configurationField.get(target);
        } catch (ReflectiveOperationException e) {

        }

        return conf;
    }

}
