package net.j4love.mybatis.kit.executor;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.ibatis.executor.Executor;


@Data
@AllArgsConstructor
public class ExecutorEnvPair {

    private DatabaseEnv env;
    private Executor executor;
}
