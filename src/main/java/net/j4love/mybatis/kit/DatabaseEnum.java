package net.j4love.mybatis.kit;

/**
 * 数据库类型枚举
 * @author he peng
 * @date 2018/9/10
 */
public enum DatabaseEnum {

    MYSQL,

    MARIADB,

    ORACLE,

    SQLSERVER,

    DB2,

    SYBASE;


}
