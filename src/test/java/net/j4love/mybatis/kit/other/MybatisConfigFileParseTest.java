package net.j4love.mybatis.kit.other;

import org.apache.ibatis.builder.xml.XMLConfigBuilder;
import org.apache.ibatis.builder.xml.XMLMapperEntityResolver;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.parsing.XPathParser;
import org.apache.ibatis.session.Configuration;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

public class MybatisConfigFileParseTest {

    InputStream inputStream;


    @Before
    public void init() throws Exception {
        inputStream = Resources.getResourceAsStream("mybatis-config.xml");
    }

    @Test
    public void parse() throws Exception {
        XMLConfigBuilder parser = new XMLConfigBuilder(inputStream, null, null);
        Configuration configuration = parser.parse();
        System.out.println(configuration);
    }

    @Test
    public void parse1() throws Exception {
        XPathParser parser = new XPathParser(inputStream, true, null, new XMLMapperEntityResolver());
        XNode rootNode = parser.evalNode("/configuration");
        XNode environmentsNode = rootNode.evalNode("environments");

        /*if (environment == null) {
            environment = context.getStringAttribute("default");
        }
        for (XNode child : context.getChildren()) {
            String id = child.getStringAttribute("id");
            if (isSpecifiedEnvironment(id)) {
                TransactionFactory txFactory = transactionManagerElement(child.evalNode("transactionManager"));
                DataSourceFactory dsFactory = dataSourceElement(child.evalNode("dataSource"));
                DataSource dataSource = dsFactory.getDataSource();
                Environment.Builder environmentBuilder = new Environment.Builder(id)
                        .transactionFactory(txFactory)
                        .dataSource(dataSource);
                configuration.setEnvironment(environmentBuilder.build());
            }
        }*/

        Set<String> envIds = new HashSet<>();
        String defaultEnvId = environmentsNode.getStringAttribute("default");
        for (XNode child : environmentsNode.getChildren()) {
            String envId = child.getStringAttribute("id");
            envIds.add(envId);
        }
        System.out.println(envIds);
    }
}
