package net.j4love.mybatis.kit.other;

import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlSchemaStatVisitor;
import com.alibaba.druid.sql.repository.SchemaRepository;
import com.alibaba.druid.sql.visitor.SchemaStatVisitor;
import com.alibaba.druid.stat.TableStat;
import com.alibaba.druid.util.JdbcConstants;
import org.junit.Test;

import java.util.List;
import java.util.Map;

public class DruidSQLParserTest {

    @Test
    public void test1() throws Exception {

        // String sql = "update t set name = 'x' where id < 100 limit 10";
        // String sql = "SELECT ID, NAME, AGE FROM USER WHERE ID = ? limit 2";
        // String sql = "select * from tablename limit 10";

        String sql = "select user from emp_table";
        String dbType = JdbcConstants.MYSQL;

        //格式化输出
        String result = SQLUtils.format(sql, dbType);
        System.out.println("format -> " + result); // 缺省大写格式
        List<SQLStatement> stmtList = SQLUtils.parseStatements(sql, dbType);

        //解析出的独立语句的个数
        System.out.println("size is:" + stmtList.size());
        for (int i = 0; i < stmtList.size(); i++) {

            SQLStatement stmt = stmtList.get(i);
            MySqlSchemaStatVisitor visitor = new MySqlSchemaStatVisitor();
            stmt.accept(visitor);

            //获取表名称
//            System.out.println("Tables : " + visitor.getCurrentTable());
            //获取操作方法名称,依赖于表名称
            System.out.println("Manipulation : " + visitor.getTables());
            //获取字段名称
            System.out.println("fields : " + visitor.getColumns());

        }
    }

    @Test
    public void test2() throws Exception {
//         String sql = "update t set name = 'x' where id < 100 limit 10";
        // String sql = "SELECT ID, NAME, AGE FROM USER WHERE ID = ? limit 2";
        // String sql = "select * from tablename limit 10";
        String sql = "select * from person\n" +
                "left join orders\n" +
                "on person.p_id = orders.p_id\n" +
                "where orders.o_id is null\n" +
                "union\n" +
                "select * from person\n" +
                "right join orders\n" +
                "on person.p_id = orders.p_id\n" +
                "where person.p_id is null " +
                "order by person.create_time";

//        String sql = "select user from emp_table";
        String dbType = JdbcConstants.MYSQL;

        //格式化输出
        String result = SQLUtils.format(sql, dbType);
        System.out.println("format -> " + result); // 缺省大写格式
        List<SQLStatement> stmtList = SQLUtils.parseStatements(sql, dbType);

        //解析出的独立语句的个数
        System.out.println("size is:" + stmtList.size());

        for (int i = 0; i < stmtList.size(); i++) {

            SQLStatement stmt = stmtList.get(i);
            System.out.println(" SQLStatement = " +  stmt.toLowerCaseString());
            SchemaStatVisitor visitor = SQLUtils.createSchemaStatVisitor(dbType);
            stmt.accept(visitor);

            //获取操作方法名称,依赖于表名称
            Map<TableStat.Name, TableStat> tables = visitor.getTables();
            System.out.println("tables : " + tables);

            for (Map.Entry<TableStat.Name, TableStat> entry : tables.entrySet()) {
                TableStat.Name key = entry.getKey();
                TableStat value = entry.getValue();
                System.out.println("table name = " + key.getName() + "  ,  ops = " + value.toString());
            }

            //获取字段名称
            System.out.println("fields : " + visitor.getColumns());

        }
    }
}
