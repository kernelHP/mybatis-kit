package net.j4love.mybatis.kit.dao;



import net.j4love.mybatis.kit.model.User;


public interface UserDAO {

    User selectByPrimaryKey(Long id);

    long selectCount();

    User getUserWithInterestById(Long id);

    User getUserByPhone(Long phone);

    Integer insert(User user);
}
