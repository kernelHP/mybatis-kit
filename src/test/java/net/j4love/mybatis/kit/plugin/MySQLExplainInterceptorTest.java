package net.j4love.mybatis.kit.plugin;

import net.j4love.mybatis.kit.dao.UserDAO;
import net.j4love.mybatis.kit.model.User;
import org.apache.ibatis.executor.BaseExecutor;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.lang.reflect.Field;


public class MySQLExplainInterceptorTest {


    SqlSessionFactory sqlSessionFactory;

    @Before
    public void init() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        this.sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }

    @Test
    public void explainTest() throws Exception {
        SqlSession session = this.sqlSessionFactory.openSession();
        UserDAO userDAO = session.getMapper(UserDAO.class);
        User user = userDAO.selectByPrimaryKey(3L);
        Assert.assertNotNull(user);
        Thread.sleep(10000);
    }

    @Test
    public void test1() throws Exception {
        Field f = BaseExecutor.class.getDeclaredField("configuration");
        System.out.println(f);
    }
}
