package net.j4love.mybatis.kit.plugin;

import net.j4love.mybatis.kit.dao.UserDAO;
import net.j4love.mybatis.kit.model.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.Date;

import static org.junit.Assert.*;

public class ReadWriteSeparateInterceptorTest {

    SqlSessionFactory sqlSessionFactory;

    @Before
    public void init() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        this.sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }

    @Test
    public void selectFromSlaveDatabase() throws Exception {
        SqlSession session = this.sqlSessionFactory.openSession();
        UserDAO userDAO = session.getMapper(UserDAO.class);
        User user = userDAO.selectByPrimaryKey(3L);
        Assert.assertNotNull(user);
        Thread.sleep(10000);
    }

    @Test
    public void transactionTest1() throws Exception {
        SqlSession session = this.sqlSessionFactory.openSession(false);
        UserDAO userDAO = session.getMapper(UserDAO.class);
        User user = new User();
        user.setUpdateTime(new Date());
        user.setNickname("star");
        user.setRealname("杰森伯恩");
        user.setPhone(13723236325L);
        user.setLoginPassword("123456789");
        user.setPayPassword("123456");
        user.setCreateTime(new Date());
        int i = userDAO.insert(user);

        User user1 = userDAO.selectByPrimaryKey(3L);
        System.out.println(user1);
        session.commit(true);
    }

    @Test
    public void transactionTest2() throws Exception {
        SqlSession session = this.sqlSessionFactory.openSession(true);
        UserDAO userDAO = session.getMapper(UserDAO.class);
        User user1 = userDAO.selectByPrimaryKey(2L);

        User user2 = userDAO.selectByPrimaryKey(3L);
        System.out.println(user1);
        session.commit(true);
    }
}