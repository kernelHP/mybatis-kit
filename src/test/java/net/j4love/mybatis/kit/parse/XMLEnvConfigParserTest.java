package net.j4love.mybatis.kit.parse;

import net.j4love.mybatis.kit.plugin.XMLEnvConfiguration;
import org.junit.Before;
import org.junit.Test;

public class XMLEnvConfigParserTest {

    XMLEnvConfiguration xmlEnvConfiguration;

    @Before
    public void init() throws Exception {
        xmlEnvConfiguration = XMLEnvConfiguration.getXmlEnvConfigParser("mybatis-config.xml");
    }

    @Test
    public void getSlaveConfigurations() {
        System.out.println(xmlEnvConfiguration.getSlaveConfigurations());
    }

    @Test
    public void getMasterConfiguration() {
        System.out.println(xmlEnvConfiguration.getMasterConfiguration());
    }

    @Test
    public void getDefaultEnvId() {
        System.out.println(xmlEnvConfiguration.getDefaultEnvId());
    }
}